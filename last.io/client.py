# -*- coding: utf-8 -*
#!/usr/bin/env python

import sys
import traceback
import requests
from hashlib import md5

import settings
from log import logger

class LastIO():
    """Last.fm client batch importer.

    """
    def __init__(self):
        self.username = settings.API_USERNAME
        self.password = settings.API_PASSWORD
        self.api_key = settings.API_KEY
        self.api_secret = settings.API_SECRET
        self.format = settings.API_FORMAT
        self.sk = settings.API_SK

        self.__session()

    def __params(self, method, payload):
        """
        Set parameters & signature method.

        Args:
            method (string): method name
            payload (dict): parameters for an specific method

        Returns:
            common parameters as dict

        """
        return {
            'method': method,
            'format': self.format,
            'api_sig': self.__sig(method, payload),
            'api_key': self.api_key
        }

    def __sig(self, method, payload):
        """
        Signature method.

        Args:
            method (string): method name
            payload (dict): parameters for an specific method to sign

        Returns:
            api signature

        """
        args = {
            'method': method,
            'api_key': self.api_key,
        }

        for k, v in payload.items():
            args[k] = v.encode('utf8')
        signature = ''

        for k in sorted(args.keys()):
            signature += k + args[k]
        signature += self.api_secret

        return md5(signature).hexdigest()

    def __session(self):
        """
        Set session.

        """
        method = 'auth.getMobileSession'
        payload = {
            'password': self.password,
            'username': self.username
        }
        params = self.__params(method, payload)

        if (self.sk):
            logger.info('Using saved session')
        else:
            request = None
            try:
                request = requests.post(settings.API_URL, data=payload, params=params)
                self.sk = request.json()['session']['key']
                logger.info('Got new session')
            except:
                if request:
                    logger.error('{url} - {code} - {body}'.format(url=request.url, code=request.status_code, body=request.text))
                else:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    logger.error('{traceback}'.format(traceback=traceback.format_exception(exc_type, exc_value, exc_traceback)))

    def ban(self, args):
        """
        Ban a track for a given user profile..

        Args:
            args (dict): banned tracks to import

        Return:
            request object

        """
        method = 'track.ban'
        payload = {
            'context': settings.__name__,
            'sk': self.sk
        }
        payload.update(args)
        params = self.__params(method, payload)

        request = None
        try:
            request = requests.post(settings.API_URL, data=payload, params=params)
            return request
        except:
            if request:
                logger.error('{url} - {code} - {body}'.format(url=request.url, code=request.status_code, body=request.text))
            else:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                logger.error('{traceback}'.format(traceback=traceback.format_exception(exc_type, exc_value, exc_traceback)))

    def love(self, args):
        """
        Love a track for a user profile.

        Args:
            args (dict): loved tracks to import

        Return:
            request object

        """
        method = 'track.love'
        payload = {
            'context': settings.__name__,
            'sk': self.sk
        }
        payload.update(args)
        params = self.__params(method, payload)

        request = None
        try:
            request = requests.post(settings.API_URL, data=payload, params=params)
            return request
        except:
            if request:
                logger.error('{url} - {code} - {body}'.format(url=request.url, code=request.status_code, body=request.text))
            else:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                logger.error('{traceback}'.format(traceback=traceback.format_exception(exc_type, exc_value, exc_traceback)))

    def scrobble(self, args):
        """
        Add a track-play to a user's profile.

        Args:
            args (dict): batch of scrobbles to import

        Return:
            request object

        """
        method = 'track.scrobble'
        payload = {
            'context': settings.__name__,
            'sk': self.sk
        }
        payload.update(args)
        params = self.__params(method, payload)

        request = None
        try:
            request = requests.post(settings.API_URL, data=payload, params=params)
            return request
        except:
            if request:
                logger.error('{url} - {code} - {body}'.format(url=request.url, code=request.status_code, body=request.text))
            else:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                logger.error('{traceback}'.format(traceback=traceback.format_exception(exc_type, exc_value, exc_traceback)))
