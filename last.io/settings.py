# -*- coding: utf-8 -*
#!/usr/bin/env python

# Application
__name__ = ''
__version__ = ''
__description__ = ''
__author__ = ''
__copyright__ = ''
__license__ = ''
__email__ = ''

# Paths
APP_ARCHIVE = 'archive'

# Actions
ACTION_SCROBBLES = 'scrobbles'
ACTION_LOVED = 'loved'
ACTION_BANNED = 'banned'

APP_ACTIONS = (
    ACTION_SCROBBLES,
    ACTION_LOVED,
    ACTION_BANNED,
)

# Request/response formats
FORMAT_JSON = 'json'

APP_FORMATS = (
    FORMAT_JSON,
)

# Last.fm API
API_URL = 'https://ws.audioscrobbler.com/2.0/'
API_FORMAT = FORMAT_JSON
API_MAX_BATCH = 50

# Authorization
API_KEY = ''
API_SECRET = ''
API_SK = ''

# Authentication
API_USERNAME = ''
API_PASSWORD = ''
