# -*- coding: utf-8 -*
#!/usr/bin/env python

import os
import json

import status
import settings
import helper
from log import logger

def setup():
    """
    Set up basic properties.

    Returns:
        result (int) status

    """
    #@TODO setup tool verify version...

    filename = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), '..',
        'package.json'
    )
    if os.path.exists(filename):
        try:
            content = helper.get_json(filename)
        except:
            logger.critical(status.CODES[status.PACKAGE_FAILED])
            return status.PACKAGE_FAILED
        else:
            logger.info('Setting up using package file')
            settings.__name__ = content.get('name', '') or settings.__name__
            settings.__version__ = content.get('version', '') or settings.__version__
            settings.__description__ = content.get('description', '') or settings.__description__
            settings.__author__ = content.get('author', '') or settings.__author__
            settings.__copyright__ = content.get('copyright', '') or settings.__copyright__
            settings.__license__ = content.get('license', '') or settings.__license__
            settings.__email__ = content.get('email', '') or settings.__email__
            return status.SUCCESS
    logger.critical(status.CODES[status.PACKAGE_NOT_FOUND])
    return status.PACKAGE_NOT_FOUND

def load():
    """
    Load user configuration file.

    Returns:
        result (int) status

    """
    name = '{name}.json'.format(name=settings.__name__.lower())
    path = (
        os.path.join(
            os.path.expanduser('~'),
            '.{name}'.format(name=name)
        ),
        os.path.join(
            os.path.dirname(os.path.abspath(__file__)), '..',
            '.{name}'.format(name=name)
        )
    )
    for filename in path:
        if os.path.exists(filename):
            try:
                content = helper.get_json(filename)
            except:
                logger.info('Using factory settings due to errors on {filename}'.format(filename=filename))
            else:
                logger.info('Loading custom settings from {filename}'.format(filename=filename))
                settings.API_USERNAME = str(content.get('api_username', '')).replace(' ', '') or settings.API_USERNAME
                settings.API_PASSWORD = str(content.get('api_password', '')).replace(' ', '') or settings.API_PASSWORD
                settings.API_KEY = str(content.get('api_key', '')).replace(' ', '') or settings.API_KEY
                settings.API_SECRET = str(content.get('api_secret', '')).replace(' ', '') or settings.API_SECRET
                settings.API_SK = str(content.get('api_sk', '')).replace(' ', '') or settings.API_SK

                api_format = str(content.get('api_format', '')).replace(' ', '')
                if (not api_format in settings.APP_FORMATS):
                    api_format = ''
                settings.API_FORMAT = api_format or settings.API_FORMAT
                return status.SUCCESS
            CONFIG_NOT_FOUND
    logger.warning('{message} - {path}', format(message=status.CODES[status.CONFIG_NOT_FOUND], path=path))
    return status.CONFIG_NOT_FOUND
