# -*- coding: utf-8 -*
#!/usr/bin/env python

import sys

import command

def main():
    return command.runner()

if __name__ == '__main__':
    sys.exit(main())
