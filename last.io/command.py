# -*- coding: utf-8 -*
#!/usr/bin/env python

import argparse

import status
import settings
import handler
import config
from log import logger

def runner():
    """
    Run application.

    Returns:
        result (int) status

    """
    state = config.setup()
    if (state != status.SUCCESS): return state

    state = config.load()
    if (state != status.SUCCESS): return state

    parser = argparse.ArgumentParser(
        prog=settings.__name__,
        description=settings.__description__
    )
    parser.add_argument('-p', '--path',
        help='directory where to obtain resources to import, should be absolute',
        default='',
    )
    parser.add_argument('-a', '--action',
        help='valid resource name to import',
        default=settings.ACTION_SCROBBLES,
        choices=settings.APP_ACTIONS,
    )

    args = parser.parse_args()
    if not args.path:
        logger.error('Invalid path - {path}'.format(path=args.path))
        return status.INVALID_ARGS
    elif not args.action in settings.APP_ACTIONS:
        logger.error('Invalid action - {action}'.format(action=args.action))
        return status.INVALID_ARGS
    else:
        logger.info('Starting {name}'.format(name=settings.__name__))
        return handler.loader(args.path, args.action)
