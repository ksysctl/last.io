# -*- coding: utf-8 -*
#!/usr/bin/env python

import logging
import settings

logger = logging.getLogger(settings.__name__)

__formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
__stream = logging.StreamHandler()
__stream.setFormatter(__formatter)
__stream.setLevel(logging.INFO)

logger.addHandler(__stream)
logger.setLevel(logging.INFO)
