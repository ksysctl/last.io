# -*- coding: utf-8 -*
#!/usr/bin/env python

SUCCESS = 0
MAX_SCROBBLES_EXCEEDED = 1
FILE_NOT_FOUND = 2
PACKAGE_NOT_FOUND = 3
PACKAGE_FAILED = 4
INVALID_ARGS = 5
CONFIG_NOT_FOUND = 6

CODES = {
    SUCCESS: 'Operation completed successful',
    MAX_SCROBBLES_EXCEEDED: 'Max scrobbles per day exceeded',
    FILE_NOT_FOUND: 'File does not exists',
    PACKAGE_NOT_FOUND: 'Package file package.json not found',
    PACKAGE_FAILED: 'Problem parsing package.json',
    INVALID_ARGS: 'Invalid argument',
    CONFIG_NOT_FOUND: 'User configuration file not found',
}
