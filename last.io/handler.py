# -*- coding: utf-8 -*
#!/usr/bin/env python

import os
import sys
import traceback
import glob
import time
import requests
from itertools import *

import status
import settings
import helper
from log import logger
from client import LastIO

def banned(data, filename):
    """
    Send banned tracks requests.

    Args:
        data (tuple): raw data as json
        filename (string): file that contains json data

    Returns:
        result (int) status

    """
    app = LastIO()

    retry = []
    i = 0
    payload = {}
    for item in data:
        payload.update({
            'artist': item['track']['artist']['name'],
            'track': item['track']['name']
        })
        logger.info('Packing banned track #{i:0>2d} {time}'.format(i=i, time=item['timestamp']['iso']))
        response = app.ban(payload)
        if not response or response.status_code != requests.codes.ok or 'error' in response.json():
            retry.append(item)
        i += 1
    if retry: # create new file w/pending tracks to retry later
        helper.backup(retry, filename)
    helper.archive(filename) # move original filename
    return status.SUCCESS

def loved(data, filename):
    """
    Send loved tracks requests.

    Args:
        data (tuple): raw data as json
        filename (string): file that contains json data

    Returns:
        result (int) status

    """
    app = LastIO()

    retry = []
    i = 0
    payload = {}
    for item in data:
        payload.update({
            'artist': item['track']['artist']['name'],
            'track': item['track']['name']
        })
        logger.info('Packing loved track #{i:0>2d} {time}'.format(i=i, time=item['timestamp']['iso']))
        response = app.love(payload)
        if not response or response.status_code != requests.codes.ok or 'error' in response.json():
            retry.append(item)
        i += 1
    if retry: # create new file w/pending tracks to retry later
        helper.backup(retry, filename)
    helper.archive(filename) # move original filename
    return status.SUCCESS

def scrobbles(data, filename):
    """
    Send batch of scrobbles splitted in chunks due to api limitations.

    Args:
        data (tuple): raw data as json
        filename (string): file that contains json data

    Returns:
        result (int) status

    """
    app = LastIO()

    retry = []
    j = 0 # batch counter
    k = 0 # counter of all items
    tracks = iter(data)
    batches = iter(lambda: tuple(islice(tracks, settings.API_MAX_BATCH)), ()) # split data in batches
    for batch in batches:
        i = 0 # item counter per batch
        payload = {}
        logger.info('Sending batch #{j} of {max} scrobbles'.format(j=j, max=len(batch)))
        for item in batch:
            payload.update({
                'artist[%s]' % i: item['track']['artist']['name'],
                'track[%s]' % i: item['track']['name'],
                'timestamp[%s]' % i: '%d' % (time.time() - 86400)
            })

            if 'album' in item:
                payload.update({
                    'album[%s]' % i: item['album']['name'],
                })
            logger.info('Packing scrobble #{i:0>2d} {time}'.format(i=i, time=item['timestamp']['iso']))
            i += 1
            k += 1

        response = app.scrobble(payload)
        if not response or response.status_code != requests.codes.ok or 'error' in response.json():
            retry.extend(batch)
        elif 'ignored' in response.json()['scrobbles']['@attr'] and int(response.json()['scrobbles']['@attr']['ignored']) > 0:
            retry.extend(batch)
            retry.extend(data[k:len(data)]) # include other remaining tracks

            helper.backup(retry, filename) # create new file w/pending tracks to retry later
            helper.archive(filename) # move original filename

            logger.critical(status.CODES[status.MAX_SCROBBLES_EXCEEDED])
            return status.MAX_SCROBBLES_EXCEEDED
        j += 1

    if retry: # create new file w/pending tracks to retry later
        helper.backup(retry, filename)
    helper.archive(filename) # move original filename
    return status.SUCCESS

def loader(path, action):
    """
    Get all json files files to import resources.

    Args:
        path (string): directory where to locate resource to import, path should be absolute
        action (string): resource name to import

    Returns:
        result (int) status

    """
    def run(filename, action):
        try:
            if os.path.exists(filename):
                getattr(
                    sys.modules[__name__],
                    '{action}'.format(action=action)
                )(helper.get_json(filename), filename)
            else:
                logger.critical('{filename} - {message}'.format(filename=filename, message=status.CODES[status.FILE_NOT_FOUND]))
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.error('{traceback}'.format(traceback=traceback.format_exception(exc_type, exc_value, exc_traceback)))

    if action == settings.ACTION_SCROBBLES:
        for filename in glob.glob(os.path.join(path, action, '*.json')):
            state = run(filename, action)
            if (state != status.SUCCESS): return state
    else:
        filename = os.path.join(path, '{action}.json'.format(action=action))
        run(filename, action)
