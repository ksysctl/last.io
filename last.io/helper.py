# -*- coding: utf-8 -*
#!/usr/bin/env python

import os
import time
import json
import shutil

import settings
from log import logger

def archive(filename):
    """
    Move processed file.

    Args:
        filename (string): file that contains json data

    """
    base, name = os.path.split(filename)
    archive_path = os.path.join(base, settings.APP_ARCHIVE)
    archive_dst = os.path.join(archive_path, name)

    if not os.path.exists(archive_path):
        os.makedirs(archive_path)

    logger.info('Moving {filename} to {destiny}'.format(filename=filename, destiny=archive_dst))
    shutil.move(filename, archive_dst)

def backup(data, filename, last=None):
    """
    Create new filename with latest items not processed.

    Args:
        data (tuple): raw data as json
        filename (string): file that contains json data
        last (last): last items not saved

    """
    base, name = os.path.split(filename)
    path = os.path.join(base, '{time}-{name}'.format(time=time.time(), name=name))

    if last is not None:
        data = data[last:len(data)]

    with open(path, 'w') as outfile:
        json.dump(data, outfile)
    logger.info('Items not processed in {filename} were saved in {path}'.format(filename=filename, path=path))

def get_json(filename):
    """
    Open json file

    Args:
        filename (string): file that contains json data

    Returns:
        return content json

    """
    logger.info('Opening {filename}'.format(filename=filename))
    json_data = open(filename)
    content = json.load(json_data)
    return content
